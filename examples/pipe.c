#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

/* Execute ls and pipe the output to sort using fork and dup2 (duplicate file descriptor) */
int main() {
    int pipe_connect[2];
    int pipeValue;
    pipeValue = pipe(pipe_connect);

    if  (pipeValue != 0) {
        printf("Pipe died with code: %d\n", pipeValue);
    }

    if (fork() == 0) {
        // first child executing ls and piping it into the pipe input (1)
        dup2(pipe_connect[1], 1);
        close(pipe_connect[0]);
        execlp("ls", "ls", 0, (char *)0);
    } else if (fork() == 0) {
        // second child listening to pipe output (0) and executing sort with the input
        dup2(pipe_connect[0], 0);
        close(pipe_connect[1]);
        execlp("sort", "sort", 0, (char *)0);
    } else {
        // father closing the pipe so children can communicate
        close(pipe_connect[0]);
        close(pipe_connect[1]);
        wait(0);
        wait(0);
    }
}