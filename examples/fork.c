#include <stdio.h>
#include <unistd.h>

int main() {
    int pid = fork();
    if (pid < 0) {
        printf("Fork died\n");
    } else if (pid > 0) {
        printf("I am the father\n");
    } else {
        printf("I am the child\n");
    }

    // executing in both
    printf("Hello, World!\n");
}