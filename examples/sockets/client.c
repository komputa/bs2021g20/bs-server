/*
	C Socket client example.
    Tries to connect to a socket on localhost:8888 and provide a simple echo interface
*/
#include <stdio.h>          //printf
#include <stdlib.h>         //exit
#include <string.h>         //strlen
#include <sys/socket.h>     //socket
#include <arpa/inet.h>      //inet_addr
#include <unistd.h>

int main(void) {
    int sock, port;
    struct sockaddr_in server;
    char address[20], portChars[4], message[1000], server_reply[2000];

    //Create socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        printf("Could not create socket\n");
    }

    printf("Socket created\n");


    // Configure remove Server
    printf("Please define the address of the server (default 127.0.0.1): ");
    fgets(address, sizeof address, stdin);

    if(strcmp(address, "\n") == 0) strcpy(address, "127.0.0.1");

    printf("Please define the port of the server (default 8888): ");
    fgets(portChars, sizeof port, stdin);

    if(strcmp(portChars, "\n") == 0) port = 8888;
    else {
        // Convert portChars to int value
        port = (int) strtol(portChars, NULL, 0);
    }

    printf("Connecting to %s:%d\n", address, port);

    server.sin_addr.s_addr = inet_addr(address);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);

    //Connect to remote server
    if (connect(sock, (struct sockaddr *) &server, sizeof(server)) < 0) {
        perror("connect failed. Error");
        exit(1);
    }

    printf("Connected\n");

    //keep communicating with server
    while (1) {
        printf("Enter 'exit()' as message to stop the client.\n");
        printf("Enter message : ");
        fgets(message, sizeof message, stdin);

        if (strcmp(message, "exit()\n") == 0) {
            printf("Closing Socket and exiting Client.\n");
            goto Quit;
        }

        //Send some data
        if (send(sock, message, strlen(message), 0) < 0) {
            printf("Send failed\n");
            exit(1);
        }

        //Receive a reply from the server
        if (recv(sock, server_reply, 2000, 0) < 0) {
            printf("recv failed\n");
            break;
        }

        printf("Server reply : ");
        printf("%s\n", server_reply);
    }

    Quit:
    close(sock);
    exit(0);
}