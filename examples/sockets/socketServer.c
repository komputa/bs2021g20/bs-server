/*
	C socket server example.
    Starts a simple echoing socket on localhost:8888
*/

#include<stdio.h>
#include <stdlib.h>    //exit
#include<string.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h>    //inet_addr
#include<unistd.h>    //write

void handleSocketConnection (int client_sock) {
    int read_size;
    char client_message[2000];

    if (client_sock < 0) {
        perror("accept failed");
        exit(1);
    }
    printf("Connection accepted\n");

    //Receive a message from client
    while ((read_size = (int) recv(client_sock, client_message, 2000, 0)) > 0) {
        printf("Received Message from Client: '%s'. \nEchoing it back to the client.\n", client_message);
        write(client_sock, client_message, strlen(client_message));
    }

    if (read_size == 0) {
        printf("Client disconnected\n");
        fflush(stdout);
    } else if (read_size == -1) {
        perror("recv failed");
    }
    exit(1);
}

int main(void) {
    int socket_desc, client_sock, c;
    struct sockaddr_in server, client;

    //Create socket
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_desc == -1) {
        printf("Could not create socket");
    }
    printf("Socket created\n");

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(8888);

    //Bind
    if (bind(socket_desc, (struct sockaddr *) &server, sizeof(server)) < 0) {
        //print the error message
        perror("bind failed. Error");
        exit(1);
    }
    printf("bind done\n");

    Listen:
    listen(socket_desc, 3);

    //Accept and incoming connection
    printf("Waiting for incoming connections...\n");
    c = sizeof(struct sockaddr_in);

    //accept connection from an incoming client
    client_sock = accept(socket_desc, (struct sockaddr *) &client, (socklen_t *) &c);
    // accept waits once a connection is created. Fork the process and continue executing in the child.
    int pid = fork();
    if (pid < 0) {
        printf("Fork failed, couldn't create a new process for the new client.\n");
        close(client_sock);
    } else if (pid == 0) {
        // continue the logic in the child
        handleSocketConnection(client_sock);
    } else {
        // main server goes back to listening for connections
        goto Listen;
    }

    exit(0);
}