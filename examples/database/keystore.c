#include <stdio.h>
#include <unistd.h>

int main() {
    int pid = fork();
    if (pid < 0) {
        printf("Fork died\n");
    } else if (pid > 0) {
        printf("I am the father\n");
        int c;
        FILE *file;
        file = fopen("../database/db.txt", "r");
        if (file) {
            printf("Father File: ");
            while ((c = getc(file)) != EOF)
                putchar(c);
        }
    } else {
        printf("I am the child\n");
        int c;
        FILE *file;
        file = fopen("../database/db.txt", "r");
        if (file) {
            printf("Child File: ");
            while ((c = getc(file)) != EOF)
                putchar(c);
        }
    }

    // executing in both
    printf("\nBoth: Hello, World!\n");
}