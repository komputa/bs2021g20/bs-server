#ifndef BS_SERVER_STORE_H
#define BS_SERVER_STORE_H

int keyExists (char *key);

void getPairByKey(char *key, int maxSize, char *value);

void setValue(char *key, char *value, char *result);

void deleteValue(char *key, char *deletedLine);

#endif //BS_SERVER_STORE_H
