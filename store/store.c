#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "../server/helpers.h"

FILE *file;


FILE * openFileByNameWithMode (char *name, char *mode) {
    char path[200];

    buildFilePath(name, path);

    return fopen(path, mode);
}

void openFileToAppend(char *name) {
    file = openFileByNameWithMode(name, "a");
}

void openFile(char *name) {
    file = openFileByNameWithMode(name, "r");
}

void closeFile() {
    fclose(file);
}

void removeFile (char *name) {
    char path[200];
    buildFilePath(name, path);
    remove(path);
}

int keyExists (char *key) {
    char path[200];
    buildFilePath(key, path);

    if (access(path, F_OK) == 0) {
        return 1;
    } else {
        return 0;
    }
}

void clearKey (char *key) {
    closeFile();
    removeFile(key);
    openFileToAppend(key);
}

void setValue(char *key, char *value, char *buffer) {
    openFileToAppend(key);

    // create key:value pair to return to caller
    strcpy(buffer, key);
    strcat(buffer, ":");
    strcat(buffer, value);
    strcat(buffer, "\n");

    if (keyExists(key)) {
        clearKey(key);
    }

    fputs(value, file);
    closeFile();
}

void getPairByKey(char *key, int maxSize, char *result) {
    char line[maxSize];

    openFile(key);

    strcpy(result, key);
    strcat(result, ":");

    if (file == NULL) {
        strcat(result, "key_nonexistent\n");
        return;
    }

    while (fgets(line, maxSize, file) != NULL) {
        // get first line and return
        strcat(result, line);
        strcat(result, "\n");
        closeFile();
        return;
    }
}

void deleteValue(char *key, char *result) {
    openFile(key);

    strcpy(result, key);
    strcat(result, ":");

    if (file == NULL) {
        strcat(result, "key_nonexistent\n");
        return;
    }

    closeFile();
    removeFile(key);

    strcat(result, "key_deleted\n");
}

