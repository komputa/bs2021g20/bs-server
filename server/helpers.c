#include<string.h>
#include<sys/socket.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include <sys/mman.h>

typedef struct {
    int id;
} pid;

typedef struct {
    pid pool[1000];
    int nextFree;
} pidList;

void addPid(pidList *pidList, int newPID) {
    pidList->pool[pidList->nextFree].id = newPID;
    pidList->nextFree = pidList->nextFree + 1;
    printf("Add Pid: %i\n", newPID);
}

int prependToMessage(char *resultMessage, char *oldMessage, char *prefix, int oldMessageSize) {
    strcat(resultMessage, prefix);
    strcat(resultMessage, oldMessage);
    return oldMessageSize + (int) strlen(prefix);
}

int appendToMessage(char *resultMessage, char *oldMessage, char *suffix, int oldMessageSize) {
    strcat(resultMessage, oldMessage);
    strcat(resultMessage, suffix);
    return oldMessageSize + (int) strlen(suffix);
}

void handleError(int status) {
    exit(status);
}

void closeSocket(int clientSock) {
    shutdown(clientSock, SHUT_RDWR);
    close(clientSock);
}

void clearString (char *string) {
    memset(string, '\0', sizeof(*string));
}

void writeToClient (int clientSock, char *message, int clearBuffer) {
    long err = send(clientSock, message, strlen(message), 0);
    if (err < 0) printf("Client write failed\n");
    if (clearBuffer == 1) clearString(message);
}

void sendMessage(int clientSock, char *message) {
    writeToClient(clientSock, message, 1);
}

void sendMessageFromString (int clientSock, char *message) {
    writeToClient(clientSock, message, 0);
}

int compareStrings(char *s1, char *s2) {
    return (strcmp(s1, s2) == 0);
}

void getKeyFromMessage(char *message, char *key) {
    char messageCopy[strlen(message)];
    char delimiter[] = " ";
    char *ptr;
    int counter = 0;

    strcpy(messageCopy, message);
    ptr = strtok(messageCopy, delimiter);

    while (ptr != NULL) {
        if (counter == 1) {
            strcpy(key, ptr);
            return;
        }
        ptr = strtok(NULL, delimiter);
        counter++;
    }
    strcpy(key, "");
}

void getValueFromMessage(char *message, char *value) {
    char messageCopy[strlen(message)];
    char delimiter[] = " ";
    char *ptr;
    int counter = 0;

    strcpy(messageCopy, message);
    ptr = strtok(messageCopy, delimiter);

    while (ptr != NULL) {
        if (counter == 2) {
            strcpy(value, ptr);
            return;
        }
        ptr = strtok(NULL, delimiter);
        counter++;
    }
    strcpy(value, "");
}

void getActionFromMessage(char *message, char *action) {
    snprintf(action, sizeof action, "%.3s", message);
}

void buildFilePath(char *name, char *result) {
    char *rawPath = "../store/db/";

    strcpy(result, rawPath);
    strcat(result, name);
    strcat(result, ".txt");
}

void writeIntToSharedMemory(void* sharedMemory, int value) {
    int intSize = sizeof(int);
    memcpy(sharedMemory, &value, intSize);
}

int readIntFromSharedMemory(void* sharedMemory) {
    return ((int*) sharedMemory)[0];
}

void* create_shared_memory(size_t size) {
    // Our memory buffer will be readable and writable:
    int protection = PROT_READ | PROT_WRITE;

    // The buffer will be shared (meaning other processes can access it), but
    // anonymous (meaning third-party processes cannot obtain an address for it),
    // so only this process and its children will be able to use it:
    int visibility = MAP_SHARED | MAP_ANONYMOUS;

    // The remaining parameters to `mmap()` are not important for this use case,
    // but the manpage for `mmap` explains their purpose.
    return mmap(NULL, size, protection, visibility, -1, 0);
}

int operationsBlocked (int clientSock, void* shared_memory_socket_id, int SHARED_MEMORY_UNSET_SOCKET_ID) {
    int current_exclusive_socket_id = readIntFromSharedMemory(shared_memory_socket_id);
    return current_exclusive_socket_id > SHARED_MEMORY_UNSET_SOCKET_ID && clientSock != current_exclusive_socket_id;
}

void revokeExclusiveAccessIfNeeded (int clientSock, void* shared_memory_socket_id, int SHARED_MEMORY_UNSET_SOCKET_ID) {
    int current_exclusive_socket_id = readIntFromSharedMemory(shared_memory_socket_id);
    if (current_exclusive_socket_id == clientSock) {
        printf("Disconnected Client had exclusive access. Revoking it.\n");
        writeIntToSharedMemory(shared_memory_socket_id, SHARED_MEMORY_UNSET_SOCKET_ID);
    }
}

void handleShutdown(int clientSock, int code, void* shared_memory_socket_id, int SHARED_MEMORY_UNSET_SOCKET_ID) {
    printf("Closing Socket %d\n", clientSock);
    closeSocket(clientSock);
    fflush(stdout);
    revokeExclusiveAccessIfNeeded(clientSock, shared_memory_socket_id, SHARED_MEMORY_UNSET_SOCKET_ID);
    exit(code);
}

void handleShutdownSignal() {
    exit(0);
}

int charIsNumber (const char *c) {
    return *c >= 48 && *c <= 57;
}

int charIsUpperCaseLetter(const char *c) {
    return *c >= 65 && *c <= 90;
}

int charIsLowerCaseLetter (const char *c) {
    return *c >= 97 && *c <= 122;
}

int charIsLetter (const char *c) {
    return charIsUpperCaseLetter(c) || charIsLowerCaseLetter(c);
}

int stringOnlyContainsAlphanumericCharacters(char *string) {
    while (*string != '\0') {
        if (!charIsNumber(string) && !charIsLetter(string)) {
            return 0;
        }
        string++;
    }
    return 1;
}

