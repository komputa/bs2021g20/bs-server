#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <sys/shm.h>

#include "helpers.h"
#include "../store/store.h"

int maxMessageSize = 2000;
void *shared_memory_socket_id;
const int SHARED_MEMORY_UNSET_SOCKET_ID = -1;
int semStore;
int messageQueueId;
pidList *sharedPidList;
char command[4];

// Create Semaphore Ops
struct sembuf lockSem = {0, -1, SEM_UNDO};
struct sembuf unlockSem = {0, 1, SEM_UNDO};

struct messageBuffer {
    long mtype;
    char mtext[1024];
    char key[1024];
    char action[1024];
};

void handleQuit(int clientSock) {
    sendMessageFromString(clientSock, "Goodbye\n");
    handleShutdown(clientSock, 0, shared_memory_socket_id, SHARED_MEMORY_UNSET_SOCKET_ID);
}

void handleInvalidCommand(int clientSock, char *messageBuffer) {
    printf("Invalid Command, echoing it back to the client.\n");

    // append > to message to indicate server response
    char prefixMessage[maxMessageSize], resultMessage[maxMessageSize];
    strcpy(prefixMessage, "");
    strcpy(resultMessage, "");
    int prefixMessageLength = prependToMessage(prefixMessage, messageBuffer, ">INVALID_COMMAND: ",
                                               (int) strlen(messageBuffer));
    appendToMessage(resultMessage, prefixMessage, "\n", prefixMessageLength);

    // send response
    sendMessage(clientSock, resultMessage);
}

void sendMessageToQueue(char *message) {
    struct messageBuffer messageBuffer;
    printf("NEXT FREE: %d\n", sharedPidList->nextFree);

    strcpy(messageBuffer.mtext, message);
    printf("TEST\n");
    printf("Sending to %d recipients\n", sharedPidList->nextFree);
    int currentPosition = 0;
    while (currentPosition < sharedPidList->nextFree) {
        int pid = sharedPidList->pool[currentPosition];
        messageBuffer.mtype = pid;
        printf("Sending to: %li\n", messageBuffer.mtype);
        msgsnd(messageQueueId, &messageBuffer, strlen(messageBuffer.mtext) + 1, 0);
        currentPosition++;
    }
}

void sendMessageTo(int pid, char *message) {
    struct messageBuffer messageBuffer;
    printf("NEXT FREE: %d\n", sharedPidList->nextFree);

    strcpy(messageBuffer.mtext, message);
    printf("TEST\n");
    printf("Sending to %d recipients\n", sharedPidList->nextFree);
    messageBuffer.mtype = pid;
    printf("Sending to: %li\n", messageBuffer.mtype);
    msgsnd(messageQueueId, &messageBuffer, strlen(messageBuffer.mtext) + 1, 0);
}

void handlePut(int clientSock, char *message) {
    char key[maxMessageSize];
    char value[maxMessageSize];

    getKeyFromMessage(message, key);

    if (strcmp(key, "") == 0 || !stringOnlyContainsAlphanumericCharacters(key)) {
        handleInvalidCommand(clientSock, message);
        return;
    }

    getValueFromMessage(message, value);

    if (strcmp(value, "") == 0 || !stringOnlyContainsAlphanumericCharacters(key)) {
        handleInvalidCommand(clientSock, message);
        return;
    }
    char pair[maxMessageSize], resultMessage[maxMessageSize];

    // Critical Area to prevent double writes
    semop(semStore, &lockSem, 1);
    setValue(key, value, pair);
    semop(semStore, &unlockSem, 1);

    sendMessageToQueue(message);

    prependToMessage(resultMessage, pair, ">PUT:", (int) strlen(pair));
    sendMessage(clientSock, resultMessage);

    pair[strcspn(pair, "\r\n")] = 0;

    printf("\nPut Pair: '%s'\n", pair);
}

void handleGet(int clientSock, char *message) {
    char key[maxMessageSize], result[maxMessageSize], resultMessage[maxMessageSize];
    getKeyFromMessage(message, key);

    if (strcmp(key, "") == 0 || !stringOnlyContainsAlphanumericCharacters(key)) {
        handleInvalidCommand(clientSock, message);
        return;
    }

    // Critical Area to prevent wrong reads
    semop(semStore, &lockSem, 1);
    getPairByKey(key, maxMessageSize, result);
    semop(semStore, &unlockSem, 1);

    prependToMessage(resultMessage, result, ">GET:", (int) strlen(result));
    sendMessage(clientSock, resultMessage);

    result[strcspn(result, "\r\n")] = 0;

    printf("Got Pair: '%s'\n", result);
}

void handleDel(int clientSock, char *message) {
    char key[maxMessageSize], result[maxMessageSize], resultMessage[maxMessageSize];

    getKeyFromMessage(message, key);

    if (strcmp(key, "") == 0 || !stringOnlyContainsAlphanumericCharacters(key)) {
        handleInvalidCommand(clientSock, message);
        return;
    }

    // Critical Area to prevent double deletes
    semop(semStore, &lockSem, 1);
    deleteValue(key, result);
    semop(semStore, &unlockSem, 1);

    sendMessageToQueue(message);

    prependToMessage(resultMessage, result, ">DEL:", (int) strlen(result));
    sendMessage(clientSock, resultMessage);

    result[strcspn(result, "\r\n")] = 0;

    printf("Deleted: '%s'\n", result);
}

void handleBeg(int clientSock) {
    // set current socket id to exclusive access
    writeIntToSharedMemory(shared_memory_socket_id, clientSock);

    sendMessageFromString(clientSock, ">BEG:EXCLUSIVE_ACCESS_GRANTED\n");

    printf("Begin exclusive access for client: '%d'\n", clientSock);
}

void handleEnd(int clientSock) {
    // set current socket id to exclusive access
    writeIntToSharedMemory(shared_memory_socket_id, SHARED_MEMORY_UNSET_SOCKET_ID);

    sendMessageFromString(clientSock, ">END:EXCLUSIVE_ACCESS_STOPPED\n");

    printf("End exclusive access for client: '%d'\n", clientSock);
}

void handleSub(int clientSock, char *message) {
    char key[maxMessageSize];

    getKeyFromMessage(message, key);

    if (strcmp(key, "") == 0) {
        handleInvalidCommand(clientSock, message);
        return;
    }

    sendMessageTo(getpid(), message);

    sendMessageFromString(clientSock, ">SUB: SUBSCRIPTION STARTED\n");
}

void handleBlockingMessage(int clientSock, char *messageBuffer, char *command) {
    char resultMessage[maxMessageSize];
    strcpy(resultMessage, ">");
    strcat(resultMessage, messageBuffer);
    strcat(resultMessage, ":ACCESS_DENIED\n");
    sendMessage(clientSock, resultMessage);

    printf("Action %s from client: '%d' was denied\n", command, clientSock);
}

void handleCommands(int clientSock, char *messageBuffer, char *command) {
    if (compareStrings(messageBuffer, "QUIT")) handleQuit(clientSock);
    else if (operationsBlocked(clientSock, shared_memory_socket_id, SHARED_MEMORY_UNSET_SOCKET_ID))
        handleBlockingMessage(clientSock, messageBuffer, command);
    else if (compareStrings(command, "PUT")) handlePut(clientSock, messageBuffer);
    else if (compareStrings(command, "GET")) handleGet(clientSock, messageBuffer);
    else if (compareStrings(command, "DEL")) handleDel(clientSock, messageBuffer);
    else if (compareStrings(command, "BEG")) handleBeg(clientSock);
    else if (compareStrings(command, "END")) handleEnd(clientSock);
    else if (compareStrings(command, "SUB")) handleSub(clientSock, messageBuffer);
    else handleInvalidCommand(clientSock, messageBuffer);
}

void handleSocketConnection(int clientSock) {
    int readSize;
    char messageBuffer[maxMessageSize];

    if (clientSock < 0) {
        perror("accept failed");
        exit(1);
    }

    // send welcome message
    sendMessageFromString(clientSock, "--------\n");
    sendMessageFromString(clientSock, "Connected to BS-Server\n");
    sendMessageFromString(clientSock,
                          "Available Commands: \n'PUT key value',\n'GET key',\n'DEL key',\n'BEG',\n'END',\n'SUB key',\n'QUIT'\n--------\n");

    //Receive a message from client
    while ((readSize = (int) recv(clientSock, messageBuffer, maxMessageSize, 0)) > 0) {
        // remove newline and carriage return from message
        messageBuffer[strcspn(messageBuffer, "\r\n")] = 0;

        fprintf(stdout, "Received Message from Client on Socket %d: '%.*s'\n", clientSock, readSize, messageBuffer);

        // Get first 3 Chars to get Command
        snprintf(command, sizeof command, "%.3s", messageBuffer);

        handleCommands(clientSock, messageBuffer, command);

        strcpy(messageBuffer, "");
    }

    if (readSize == 0) {
        printf("Client disconnected\n");
    } else if (readSize == -1) {
        perror("recv failed");
    }

    handleShutdown(clientSock, 0, shared_memory_socket_id, SHARED_MEMORY_UNSET_SOCKET_ID);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"

int main(void) {
    int port, socketDesc, clientSock, c;
    struct sockaddr_in server, client;
    key_t messageQueueKey;

    if ((messageQueueKey = ftok("../messageQueue", 65)) < 0) {
        perror("ftok()");
        return EXIT_FAILURE;
    }

    if ((messageQueueId = msgget(messageQueueKey, 0644 | IPC_CREAT)) < 0) {
        perror("msgget()");
        return EXIT_FAILURE;
    }

    int sharedSubscriptionListID = shmget(IPC_PRIVATE, sizeof(pidList), IPC_CREAT | 0644);
    if (sharedSubscriptionListID == -1) {
        puts("Some error occurred while setting up shared memory for subscriptionList.");
        exit(1);
    }
    sharedPidList = (pidList *) shmat(sharedSubscriptionListID, 0, 0);
    sharedPidList->nextFree = 0;

    printf("NEXT FREE: %d\n", sharedPidList->nextFree);

    // Init Semaphores
    semStore = semget(IPC_PRIVATE, 1, IPC_CREAT | 0777);

    if (semStore == -1) {
        printf("Could not create Semaphore");
        handleError(1);
    }

    unsigned short marker[1] = {1};
    semctl(semStore, 1, SETALL, marker);

    // Shared Memory anlegen, um exklusiven Zugriff eines bestimmten Clients zu ermöglichen
    shared_memory_socket_id = create_shared_memory(4);
    writeIntToSharedMemory(shared_memory_socket_id, SHARED_MEMORY_UNSET_SOCKET_ID);

    signal(SIGINT, handleShutdownSignal);

    //Create socket
    socketDesc = socket(AF_INET, SOCK_STREAM, 0);

    if (socketDesc < 0) {
        printf("Could not create socket");
        handleError(1);
    }

    // Set SO_REUSEADDR flag on Socket so the port will be freed up instantly after server shutdown
    if (setsockopt(socketDesc, SOL_SOCKET, SO_REUSEADDR, &(int) {1}, sizeof(int)) < 0)
        printf("Could not set SO_REUSEADDR flag");

    printf("Socket created\n");

    port = 5678;

    printf("Starting server on port %d\n", port);

    //Prepare the sockaddr_in structure for the Server
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);

    //Bind the Socket to the socket Address of the server
    if (bind(socketDesc, (struct sockaddr *) &server, sizeof(server)) < 0) {
        //print the error message
        perror("bind failed. Error");
        handleError(1);
    }

    printf("bind done\n");

    //Listen to incoming connections, with a buffer of 3 that can wait while the server processes the connection
    listen(socketDesc, 3);
    c = sizeof(struct sockaddr_in);

    Listen:
    //Accept and incoming connection
    printf("Waiting for incoming connections...\n");

    // accept connection from an incoming client
    // accept waits till a connection is created. Then Fork the process and continue executing in the child.
    clientSock = accept(socketDesc, (struct sockaddr *) &client, (socklen_t *) &c);

    //add client to PidList
    printf("NEXT FREE: %d\n", sharedPidList->nextFree);

    printf("Connection accepted on Socket %d\n", clientSock);

    int pid = fork();
    if (pid < 0) {
        printf("Fork failed, couldn't create a new process for the new client.\n");
        handleShutdown(clientSock, 0, shared_memory_socket_id, SHARED_MEMORY_UNSET_SOCKET_ID);
    } else if (pid == 0) {
        // fork again to create two processes.
        // One to handle the connection
        // One to listen to messages
        int pid2 = fork();

        if (pid2 < 0) {
            printf("Fork failed, couldn't create a new process to listen to messages.\n");
            handleShutdown(clientSock, 0, shared_memory_socket_id, SHARED_MEMORY_UNSET_SOCKET_ID);
        } else if (pid2 == 0) {
            // child listens to messages
            addPid(sharedPidList, getppid());
            char subKeys[1024][1024];
            int subKeysIndex = 0;
            printf("WAITING: %d\n", getppid());
            while (1) {
                struct messageBuffer newMessage;
                char key[1024];
                msgrcv(messageQueueId, &newMessage, 1024, getppid(), 0);

                printf("Received: %s.\n", newMessage.mtext);

                getKeyFromMessage(newMessage.mtext, key);
                getActionFromMessage(newMessage.mtext, command);

                if (strcmp(command, "SUB") == 0) {
                    printf("Listen to: %s\n", key);
                    strcpy(subKeys[subKeysIndex], key);
                    subKeysIndex++;
                }

                if (strcmp(key, "") != 0) {
                    int counter = 0;
                    while (counter < subKeysIndex) {
                        if (strcmp(subKeys[counter], key) == 0 && strcmp(command, "SUB") != 0) {
                            char finalMessage[maxMessageSize], prefixMessage[maxMessageSize], message[maxMessageSize];

                            strcpy(prefixMessage, "");
                            strcpy(finalMessage, "");

                            strcpy(message, newMessage.mtext);

                            prependToMessage(prefixMessage, message, ">", (int) strlen(message));
                            appendToMessage(finalMessage, prefixMessage, "\n", (int) strlen(prefixMessage));

                            sendMessageFromString(clientSock, finalMessage);

                        }
                        counter++;
                    }
                }
            }
        } else {
            // parent handles connection
            handleSocketConnection(clientSock);
        }
    } else {
        // main server goes back to listening for connections
        goto Listen;
    }

    exit(0);
}

#pragma clang diagnostic pop