#ifndef BS_SERVER_HELPERS_H
#define BS_SERVER_HELPERS_H

typedef struct {
    int pool[1000];
    int nextFree;
} pidList;

void getActionFromMessage(char *message, char *action);

void addPid(pidList *pidList, int newPID);

int prependToMessage(char *resultMessage, char *oldMessage, char *prefix, int oldMessageSize);

int appendToMessage(char *resultMessage, char *oldMessage, char *suffix, int oldMessageSize);

void handleError(int status);

void closeSocket(int clientSock);

void clearString (char *string);

void sendMessageFromString (int clientSock, char *message);

void sendMessage(int clientSock, char *message);

void handleShutdown(int clientSock, int code, void* shared_memory_socket_id, int SHARED_MEMORY_UNSET_SOCKET_ID);

void handleShutdownSignal();

int compareStrings(char *s1, char *s2);

void getKeyFromMessage(char *message, char *key);

void getValueFromMessage(char *message, char *value);

void buildFilePath (char *name, char *result);

void writeIntToSharedMemory (void* sharedMemory, int value);

int readIntFromSharedMemory (void* sharedMemory);

void* create_shared_memory(size_t size);

int operationsBlocked (int clientSock, void* shared_memory_socket_id, int SHARED_MEMORY_UNSET_SOCKET_ID);

int stringOnlyContainsAlphanumericCharacters(char *string);

#endif //BS_SERVER_HELPERS_H
