# BS Server 

The Server for the Betriebssysteme und Verteilte Systeme SoSe2021 Praktikum from Gruppe 20 (TH Köln). 

Is should contain a Server in C where Clients can connect via TCP and drop files on the server. 
The Commands should include write, read and delete. Files will be persistent.


# Interfacing with the server
Use `telnet localhost <port>`. Replace `<port>` with the one defined when starting bs-server. Default is 5678.


# Install
-make sure you have a running c environment

### CLion
-press the green arrow in Clion

### Manual CMake.
#### There are a few scripts available for doing the following steps
To use these you might need to run `chmod +x <script>` for each. e.g. ``chmod +x runServer``
- building the Cmake Directory: `./makeCmake`
- Running the server: `./runServer`
- Running any target from the CMakeList: `./runTarget <target>`

#### Manual Steps:
Create the CMake Directory. **You only need this once**: `cmake -B ./CMake`

Rebuild the given target: `cmake --build ./CMake --target <target> -- -j 6`

Execute the target: `./Cmake/<target>`

Valid targets are in CMakeLists.txt
At the moment:
- `bs-server` (The main server)
- `fork` (Fork example)
- `pipe` (Pipe example)
- `server` (server example)
- `client` (client example)
